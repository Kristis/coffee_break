var data = {
labels: ['1920', '1925', '1930', '1935', '1940', '1945', '1950', '1955', '1960', '1965', '1970', '1975', '1980', '1985', '1990', '1995', '2000', '2005', '2010', '2015'],
series: 
[{name: "namie", data: [0.24, 0.28, 0.14, 0.18, 0.17, 0.13, 0.30, 0.27, 0.24, 0.23, 0.19, null, 0.35, 0.22, 0.20, 0.24, 0.18, 0.15, 0.17, 0.18]},
{name: "kitur", data: [1.81, 2.76, 3.62, 3.35, 1.73, 2.01, 1.50, 0.90, 1.63, 1.15, 1.56, 1.57, 1.32, 3.37, 1.46, 2.04, 2.75, 2.75, 1.66, 2.70]},
]}; 
var options = {low: 0, showArea: true,	height: 300, axisY: {labelInterpolationFnc: function(value) {return '$' + value;}}};
var labels = {};
new Chartist.Line('.ct-chart', data, options, labels);