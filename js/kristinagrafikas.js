var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'horizontalBar',

    // The data for our dataset
    data: {
        labels: ["Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis", "Sekmadienis"],
        datasets: [{
            label: "Išgeriami kavos puodeliai per savaitę",
            backgroundColor: 'rgb(102, 0, 0)',
            borderColor: 'rgb(255, 99, 132)',
            data: [3, 2, 2, 3, 4, 1, 0],
        }]
    },

    // Configuration options go here
    options: {
		
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});